{ stdenv
, fetchFromGitLab
, makeWrapper
, workadventure-messages
, yarn2nix-moretea
, lib
, environment ? {}
, ... }:

let
  envVar = (name: lib.optionalAttrs (environment ? ${name}) { ${name} = environment.${name}; });

  envVars = envVar "DEBUG_MODE"
         // envVar "API_URL"
         // envVar "UPLOADER_URL"
         // envVar "ADMIN_URL"
         // envVar "MAPS_URL"
         // envVar "API_HOST"
         // envVar "UPLOADER_HOST"
         // envVar "ADMIN_HOST"
         // envVar "MAPS_HOST"
         // envVar "API_PROTOCOL"
         // envVar "UPLOADER_PROTOCOL"
         // envVar "ADMIN_PROTOCOL"
         // envVar "MAPS_PROTOCOL"
         // envVar "TURN_SERVER"
         // envVar "TURN_USER"
         // envVar "TURN_PASSWORD"
         // envVar "JITSI_URL"
         // envVar "JITSI_PRIVATE_MODE";
in yarn2nix-moretea.mkYarnPackage (rec {
  pname = "workadventurefront";
  version = "unstable";

  src = fetchFromGitLab
    {
      owner = "fediventure";
      repo = "workadventure";
      rev = "1bcf0757fd5203132edd17fc806d81e7699111a7";
      sha256 = "0wwgfrcrpimpxjhk073yxlkcp1p0bn0glxd9wisyq5kv8s7jw1yz";
    } + "/front";

  # NOTE: this is optional and generated dynamically if omitted
  yarnNix = ./yarn.nix;

  nativeBuildInputs = [ makeWrapper ];

  dontStrip = true;

  buildPhase = ''
    mkdir -p $out
    ln -s ${workadventure-messages.outPath}/generated deps/${pname}/src/Messages/generated
    HOME=$TMPDIR yarn --offline run build
    cp -r deps/${pname}/dist/ $out/
  '';

  distPhase = ":";
  installPhase = ":";
} // envVars)
